/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 

Курс валют НБУ з датою на який день, 

героїв зоряних війн, 

список справ з https://jsonplaceholder.typicode.com/ 
//виводити які з можливістю редагування при натискані 
*/
let counter = 0;
let page = 1;

const req = async (url) => {
    document.querySelector(".box_loader").classList.add("show")
    const data = await fetch(url);
    return await data.json();
}

const nav = document.querySelector(".nav")
    .addEventListener("click", (e) => {
        if (e.target.dataset.link === "nbu") {
            // counter = 0;
            page = 1;
            req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
                .then((info) => {
                    show(info, false)
                })
        } else if (e.target.dataset.link === "star") {
            // counter = 0;
            page = 1;
            // вивести всі 60 планет з https://swapi.dev/api/planets/
            req("https://swapi.dev/api/planets/")
                .then((info) => {
                    show(info.results, true);
                    page = info.next.substring(info.next.lastIndexOf("?page=") + 6, info.next.length);
                    
                    return req(info.next).then(info => {
                    
                        show(info.results, true);
                        page = info.next.substring(info.next.lastIndexOf("?page=") + 6, info.next.length);
                    
                        return req(info.next).then(info => {
							show(info.results, true);
							page = info.next.substring(info.next.lastIndexOf("?page=") + 6, info.next.length);
					
                            return req(info.next).then(info => {
											
								show(info.results, true);
								page = info.next.substring(info.next.lastIndexOf("?page=") + 6, info.next.length);
						
                                    return req(info.next).then(info => {
									    show(info.results, true);
										page = info.next.substring(info.next.lastIndexOf("?page=") + 6, info.next.length);
										
                                        return req(info.next).then(info => {
										
												show(info.results, true);
										});
						            });
					        });
                        });
                    
                    })
    		});


            
        } else if (e.target.dataset.link === "todo") {
            page = 1;
            req("https://jsonplaceholder.typicode.com/todos")
                .then((info) => {
                    show(info, false)
            })
        } else {

        }
    })

function show(data = [], flagAppend = false) {
    if (!Array.isArray(data)) return;

    const tbody = document.querySelector("tbody");

    if (page === 1) {
        tbody.innerHTML = "";
    } 

    if (!flagAppend) {
        tbody.innerHTML = ""
        // counter = 0;
    } 
    const newArr = data.map(({ txt, rate, exchangedate, title, completed, name, diameter, terrain }, i) => {
        // console.log(page, i, (page - 1) * 10 + i + 1);
        // debugger
       
			return {
				id: (page-1)*10+i+1,
				name: txt || title || name,
				info1: rate || completed || diameter,
				info2: exchangedate || terrain || "тут пусто",
			};
		});

    newArr.forEach(({ name, id, info1, info2 }) => {
        tbody.insertAdjacentHTML("beforeend", `
        <tr> 
        <td>${id}</td>
        <td>${name}</td>
        <td>${info1}</td>
        <td>${info2}</td>
        </tr>
        `)
    })

    document.querySelector(".box_loader").classList.remove("show")
}