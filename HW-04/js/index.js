'use strict'
// # 1
// Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.

const sourceArr = [1, 2, 3, 4, 5, 6, 7];
let targetArr = [];

function secondFunct(a) {
    return a * 5;
}

function map(arr, funct) {
    let arr2 = [];
    if (Array.isArray(arr)) {
        // console.log(arr.length);
        for (let i = 0; i < arr.length; i++) {
            arr2[i] = (funct(arr[i]));
        }
    } else {
        console.error("First argument is not an array");
    };    
    return arr2;
}

targetArr = map(sourceArr, secondFunct);
document.getElementById("out").innerHTML = `#1 </br>Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый элемент массива этой функцией, возвращая новый массив. </br></br>`;
document.getElementById("out").innerHTML += `Вхідний масив ${sourceArr} </br>`;
document.getElementById("out").innerHTML += `Функція ${secondFunct} </br>`;
document.getElementById("out").innerHTML += `Отриманий масив ${targetArr} </br>`;
document.getElementById("out").innerHTML += `<hr/>`;
// document.write(`${sourceArr} </br>  ${targetArr} </br>`);

// # 2
// Перепишите функцию, используя оператор '?' или '||'
// Следующая функция возвращает true, если параметр age больше 18. В ином случае она задаёт вопрос confirm и возвращает его результат.
// 1	function checkAge(age) {
// 2	if (age > 18) {
// 3	return true;
// 4	} else {
// 5	return confirm('Родители разрешили?');
// 6	} }

let age = parseInt(prompt("Вкажіть вік")),
    parentsOk = false,
    finalResult = false;

function checkAge(age) {
    return age > 18 ? true : parentsOk = confirm('Батьки дозволили?');
}


if (!isNaN(age) && (age > 0)&& (age<130)) {
finalResult = checkAge(age);
    document.getElementById("out2").innerHTML = `#2 </br>Перепишите функцию checkAge(age) используя оператор '?' или '||'. </br></br>`;
    
document.getElementById("out2").innerHTML += `Вік ${age} </br>`;
    if (age > 18) {
        document.getElementById("out2").innerHTML += `Згода батьків непотрібна </br>`;
    } else {
        document.getElementById("out2").innerHTML += `Згода батьків ${parentsOk} </br>`;
    }
document.getElementById("out2").innerHTML += `Дозвіл продовжувати ${finalResult} </br >`;

    
} else {
        document.getElementById("out2").innerHTML = `Введено нечислове або невірне значення віку</br>`;
    
};

document.getElementById("out2").innerHTML += `<hr/>`;
