/*
Робимо роботу до 3х владеностей (дії). Тобто не більге ніж 
        .one>div^a - видасть 
        <div class="one">
            <div></div>
        </div>
        <a href=""></a>
        .one>div^a>span - вже помилка.

        . - класс
        # - id
        > 1й рівень вкладеності
        div - та інші назви тегів
        {} - текст
        на навій строці нові інструкуці (Якщо в тексейрі перейшли на нову строку, то видаємо як нову вкладеність)

        .class>a*3
        #class>a.control*3
        div.class>a.control*3^img*2

*/

const textAreaText = document.querySelector("#text-area");
textAreaText.innerHTML = "";
textAreaText.value = "";
// let regexp = /\.\>\+\^\*/gi;
let reg;
let dividersArr = ["\\.", "\\>", "\\+", "\\^", "\\*","\\n"];
let resultArr = [];
let dividersPos = [];
let resultDot, resultUp, resultStar;
		let preTag;
		let postTag;
let prevInd;
        
textAreaText.addEventListener("keydown", event => {
	// console.log(textAreaText.value);
	if (event.code === "Enter") {
		console.dir(event.code);
		resultArr = [];
		dividersPos = [];
		// for (i = 0; i < textAreaText.value.length;i++){
		// result = /\./ig.exec(textAreaText.value)
		// console.log(`Найдено ${result[0]} на позиции ${result.index}`);
		// }
		/*
		let resultDot = textAreaText.value.matchAll(/\./gi);
		resultDot = Array.from(resultDot);
		// console.dir(resultDot);
		// console.dir(resultDot[0].index, resultDot.length);
		for (i = 0; i < resultDot.length; i++) {
			resultArr[resultDot[i].index] = { index: resultDot[i].index, char: ".", tag: "" };
		}
		// console.dir(resultArr);

		let resultUp = textAreaText.value.matchAll(/\^/gi);
		resultUp = Array.from(resultUp);
		// console.dir(resultUp);
		// console.dir(resultUp[0].index, resultUp.length);
		for (i = 0; i < resultUp.length; i++) {
			resultArr[resultUp[i].index] = { index: resultUp[i].index, char: "^", tag: "" };
		}
        console.dir(resultArr);
*/


		for (j = 0; j < dividersArr.length; j++) {
			reg = new RegExp(`${dividersArr[j]}`, `gi`);
			resultDot = textAreaText.value.matchAll(reg);
			resultDot = Array.from(resultDot);
			// console.dir(resultDot);
			// console.dir(resultDot[0].index, resultDot.length);
			// debugger
			for (i = 0; i < resultDot.length; i++) {
				resultArr[resultDot[i].index] = { index: resultDot[i].index, char: `${dividersArr[j]}`[1], tag: "" };
			}
			// console.dir(resultArr);
		}

		//  debugger;
		prevInd = -1;
		var j = 0;
		// for (i = 0; i < resultArr.length; i++) {
		for (i = 0; i < textAreaText.value.length; i++) {
			if (i >= 0 && resultArr[i]) {
                    
                if (resultArr[i - 1] && resultArr[i].index - resultArr[i - 1].index === 1) {
                    preTag = "div";  //textAreaText.value.substring(prevInd + 1, resultArr[i].index);{
                    resultArr[i] = {
                        index: resultArr[i].index,
                        char: resultArr[i].char,
                        tag: preTag, //"div",
                        postTag: "", //textAreaText.value.substring(resultArr[j].index + 1, dividersPos[1].index),
                    };
                } else {
                    preTag = textAreaText.value.substring(prevInd + 1, resultArr[i].index);
                    resultArr[i] = { index: resultArr[i].index, char: resultArr[i].char, info: preTag };
                }
                dividersPos[j] = {
                    index: resultArr[i].index, char: resultArr[i].char, tag: preTag, postTag: "",
                };
				j++;

				prevInd = i;
			}
		}

		console.dir(resultArr);
		// console.dir(dividersPos);

		for (j = 0; j < dividersPos.length; j++) {
			if (dividersPos[0].char === "." || dividersPos[0].char === "#") {
				dividersPos[0] = {
					index: dividersPos[0].index,
					char: dividersPos[0].char,
					tag: "div",
					postTag: textAreaText.value.substring(dividersPos[0].index + 1, dividersPos[1].index),
				};
			} else {
				dividersPos[j] = {
					index: dividersPos[j].index,
					char: dividersPos[j].char,
					tag: dividersPos[j].tag,
					postTag: "",
				};
            }
            // if (dividersPos[j].index - dividersPos[j - 1].index === 1) {
            //     dividersPos[j] = {
            //         index: dividersPos[j].index,
            //         char: dividersPos[j].char,
            //         tag: "div",
            //         postTag: textAreaText.value.substring(dividersPos[j].index + 1, dividersPos[1].index),
            //     }
            // }
              
		}
		// console.dir(dividersPos);
		
		dividersPos.push({
			index: dividersPos[dividersPos.length - 1].index + 1,
			char: "",
			tag: textAreaText.value.substring(dividersPos[dividersPos.length - 1].index + 1),
			postTag: "",
		});
		console.dir(dividersPos);
	}

	// console.dir(event.code, event.key, textAreaText.value);
});
