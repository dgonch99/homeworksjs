// Реалізуйте програму перевірки телефону
// * Використовуючи JS Створіть поле для введення телефону та кнопку збереження
// * Користувач повинен ввести номер телефону у форматі 000-000-00-00
// * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input.

window.onload = ()  => {

    let inputTel = document.createElement("input");
    inputTel.setAttribute("type", "text");
    inputTel.setAttribute("placeholder", "Введить номер телефону у форматі 000-000-00-00");
    inputTel.setAttribute("id", "input");

    let drawButton = document.createElement("input");
    drawButton.setAttribute("type", "button");
    drawButton.setAttribute("value", "Зберегти");
    drawButton.setAttribute("id", "draw-button");
    
    const divA = document.querySelector(".out");
    // console.dir(divA);
    // debugger
    divA.append(inputTel);
    divA.append(drawButton);


    drawButton.onclick = () => {
        console.dir(inputTel.value);
        let inpt1 = inputTel.value;
        // console.dir(typeof (inpt1));
        let pattern1 = new RegExp("\\d\\d\\d-\\d\\d\\d-\\d\\d-\\d\\d");
        // let pattern1 = /\d\d\d-\d\d\d-\d\d-\d\d/;
        let result = pattern1.test(inputTel.value);
        // console.dir(result);
        // debugger
        if (result) {
            inputTel.classList.add("green");
            window.open("https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg", "_blank");
            // window.location = " https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
        } else {
            inputTel.classList.remove("green");
            let errString= `<div> Помилка ввода номера по шаблону 000-000-00-00 </div>`
            inputTel.insertAdjacentHTML("beforebegin", errString);
            let errDiv = inputTel.parentElement.childNodes[1];
            // console.dir(errDiv);
            // debugger
            errDiv.classList.add("red");

        }

    };


}