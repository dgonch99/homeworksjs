import { makeTag } from "./index.js";

let flagValid = false;

function today() {
    //yyyy-mm-dd hh:mm:ss
    const date = new Date();
    return `${date.getFullYear()}${date.getMonth() < 10 ? "0" + (date.getMonth()+1) : (date.getMonth()+1)}${date.getDate() < 10 ? "0" + date.getDate() : date.getDate()}${date.getHours() < 10 ? "0" + date.getHours() : date.getHours()}${date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()}${date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds()}`;
}


const secondPage = document.querySelector("body");
secondPage.innerHTML = "";

const regPage = makeTag("div", secondPage, "reg_page");

const regPageImage = makeTag("img", regPage, "reg_img");
regPageImage.src = "./images/Figure.png";
regPageImage.setAttribute("alt", "login image");

const regForm = makeTag("div", regPage, "reg_form");

const formReg = makeTag("form", regPage, "form_reg");

const formHeader = makeTag("h2", formReg,"reg_header");
formHeader.innerHTML = "Registration form";

const userSpan = makeTag("div", formReg, "input_span");
const nameLabel = makeTag("label", userSpan, "input_label");
nameLabel.innerHTML = "Name";
nameLabel.setAttribute("for", "username");
const nameInput = makeTag("input", userSpan, "input_field", "username");
nameInput.setAttribute("type", "text");
nameInput.setAttribute("placeholder", "Please enter your name");
// console.dir(nameInput);
nameInput.addEventListener("blur", (e) => {
    // debugger
    if (Number(e.target.value.length) > 0 && /^[а-яА-Яa-zA-Z -іїєґ]\D+$/i.test(e.target.value)) {
			e.target.classList.remove("error");
		e.target.classList.add("valid");
		flagValid = true;
		regButton.disabled = false;
	} else {
		flagValid = false
		regButton.disabled = true;
			e.target.classList.add("error");
			e.target.classList.remove("valid");
		}
})

const birthSpan = makeTag("div", formReg, "input_span");
const birthLabel = makeTag("label", birthSpan, "input_label");
birthLabel.innerHTML = "Date of Birth";
birthLabel.setAttribute("for", "birthday");
const birthInput = makeTag("input", birthSpan, "input_field", "birthday");
birthInput.setAttribute("type", "date");
birthInput.setAttribute("placeholder", "dd.mm.yyyy");
birthInput.addEventListener("change", e => {
	// debugger
	let d1 = new Date(birthday.value)
	let d2 = new Date();
	// console.dir(d1 > d2);
	// debugger
	if (Number(e.target.value.length) > 0 && d2>d1) {
		e.target.classList.remove("error");
		e.target.classList.add("valid");
		flagValid = true
		regButton.disabled = false;
	} else {
		e.target.classList.add("error");
		e.target.classList.remove("valid");
		flagValid = false
		regButton.disabled = true;
	}
});

const parentSpan = makeTag("div", formReg, "input_span");
const parentLabel = makeTag("label", parentSpan, "input_label");
parentLabel.innerHTML = "Father's/Mother's Name";
parentLabel.setAttribute("for", "parent");
const parentInput = makeTag("input", parentSpan, "input_field", "parent");
parentInput.setAttribute("placeholder", "Please enter your mother or father name");
parentInput.setAttribute("type", "text");
parentInput.addEventListener("blur", e => {
	// debugger;
	if (Number(e.target.value.length) > 0 && /^[а-яА-Яa-zA-Z -іїєґ]\D+$/i.test(e.target.value)) {
		e.target.classList.remove("error");
		e.target.classList.add("valid");
		flagValid = true;
		regButton.disabled = false;
	} else {
		e.target.classList.add("error");
		e.target.classList.remove("valid");
		flagValid = false;
		regButton.disabled = true;
	}
});


const emailSpan = makeTag("div", formReg, "input_span");
const emailLabel = makeTag("label", emailSpan, "input_label");
emailLabel.innerHTML = "Email";
emailLabel.setAttribute("for", "email");
const emailInput = makeTag("input", emailSpan, "input_field", "email");
emailInput.setAttribute("placeholder", "Enter valid email address");
emailInput.setAttribute("type", "email");
emailInput.setAttribute("placeholder", "John.Jacson@yahoo.com");
emailInput.addEventListener("blur", e => {
	// debugger;
	if (Number(e.target.value.length) > 0 && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/i.test(e.target.value)) {
		e.target.classList.remove("error");
		e.target.classList.add("valid");
		flagValid = true;
		regButton.disabled = false;
	} else {
		e.target.classList.add("error");
		e.target.classList.remove("valid");
		flagValid = false;
		regButton.disabled = true;
	}
});


const mobileSpan = makeTag("div", formReg, "input_span");
const mobileLabel = makeTag("label", mobileSpan, "input_label");
mobileLabel.innerHTML = "Mobile number";
mobileLabel.setAttribute("for", "mobile");
const mobileInput = makeTag("input", mobileSpan, "input_field", "mobile");
mobileInput.setAttribute("type", "tel");
mobileInput.setAttribute("placeholder", "+380441234567");
mobileInput.addEventListener("blur", e => {
	// debugger;
	if (
		Number(e.target.value.length) > 0 &&
		/^[\+]?[0-9]{12}$/im.test(e.target.value)
		// /^[\+]?[(]?[0-9]{2,3}[)]?[-\s\.()]?[0-9]{2,3}[)-\s\.]?[0-9]{4,7}$/im.test(e.target.value)
	) {
		e.target.classList.remove("error");
		e.target.classList.add("valid");
		flagValid = true;
		regButton.disabled = false;
	} else {
		e.target.classList.add("error");
		e.target.classList.remove("valid");
		flagValid = false;
		regButton.disabled = true;
	}
});

let passw = "";
let message = "";
const passw1Span = makeTag("div", formReg, "input_span");
const passw1Label = makeTag("label", passw1Span, "input_label");
passw1Label.innerHTML = "Password";
passw1Label.setAttribute("for", "passw1");
const passw1Input = makeTag("input", passw1Span, "input_field", "passw1");
passw1Input.setAttribute("type", "password");
passw1Input.setAttribute("placeholder", "1+ digits, 1+ uppercase, 1+ lowercase, 8+ chars long");
passw1Input.addEventListener("blur", e => {
	// debugger;
	if (Number(e.target.value.length) > 0 && /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(e.target.value)) {
		e.target.classList.remove("error");
		e.target.classList.add("valid");
		passw = e.target.value;
		flagValid = true;
		regButton.disabled = false;
	} else {
		e.target.classList.add("error");
		e.target.classList.remove("valid");
		flagValid = false;
		regButton.disabled = true;
	}
});

// console.dir(message);
const passw2Span = makeTag("div", formReg, "input_span");
const passw2Label = makeTag("label", passw2Span, "input_label");
passw2Label.innerHTML = "Re-enter password";
passw2Label.setAttribute("for", "passw2");
const passw2Input = makeTag("input", passw2Span, "input_field", "passw2");
passw2Input.setAttribute("type", "password");
passw2Input.setAttribute("placeholder", "1+ digit, 1+ uppercase, 1+ lowercase, 8+ chars long");
passw2Input.addEventListener("blur", e => {
	// debugger;
	if (
		Number(e.target.value.length) > 0 && /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(e.target.value) && e.target.value === passw
	) {
		e.target.classList.remove("error");
		e.target.classList.add("valid");
		flagValid = true;
		regButton.disabled = false;
        if (message) {
            message.classList.remove("error");
            message.display = "none";
        }
	} else {
		e.target.classList.add("error");
		e.target.classList.remove("valid");
		flagValid = false;
		regButton.disabled = true;
        if (!message) {
            message = makeTag("div", passw2Span, "error");
        }
        message.innerHTML = "Passwords not match";
		message.display = "block";
	}
});


const homeSpan = makeTag("div", formReg, "input_span");
const homeLabel = makeTag("label", homeSpan, "input_label");
homeLabel.innerHTML = "Home phone number";
homeLabel.setAttribute("for", "home");
const homeInput = makeTag("input", homeSpan, "input_field", "home");
homeInput.setAttribute("type", "tel");
homeInput.setAttribute("placeholder", "+380441234567");
homeInput.addEventListener("blur", e => {
	// debugger;
	if (
		Number(e.target.value.length) > 0 &&
		/^[\+]?[0-9]{12}$/im.test(e.target.value)
		// /^[\+]?[(]?[0-9]{2,3}[)]?[-\s\.()]?[0-9]{2,3}[)-\s\.]?[0-9]{4,7}$/im.test(e.target.value)
	) {
		e.target.classList.remove("error");
		e.target.classList.add("valid");
		flagValid = true;
		regButton.disabled = false;
	} else {
		e.target.classList.add("error");
		e.target.classList.remove("valid");
		flagValid = false;
		regButton.disabled = true;
	}
});

class Cred {
	constructor({ usename, birthday, parent, email, mobile, password, home, id }) {
		this.username = nameInput.value;
		this.birthday = birthInput.value;
		this.parent = parentInput.value;
		this.email = emailInput.value;
		this.mobile = mobileInput.value;
		this.password = passw1Input.value;
		this.home = homeInput.value;
		this.id =id();
	}
};


const [...inputs] = document.querySelectorAll(".form_reg input");

// console.dir(inputs);
// debugger

const data = {
	id: today,
};

inputs.forEach(input => {
	data[input.id] = input.value;
});

const regButton = makeTag("button", formReg, "btn_login");
regButton.innerHTML = "<div>Submit</div>";
// debugger

if (flagValid) {
	regButton.setAttribute("disabled","false")
	regButton.disabled = false

} else {
	regButton.setAttribute("disabled", "true");
	regButton.disabled = true;
}

if (!sessionStorage.credentials) {
    sessionStorage.credentials = JSON.stringify([])
}
// console.log("1");
// regButton.addEventListener("click",() =>{
regButton.onclick = (e) => {
	const credentials = JSON.parse(sessionStorage.credentials);
	// prompt("2");
	credentials.push(new Cred(data));

		// console.dir(credentials);
		// debugger;

			// prompt("3");
		
		sessionStorage.credentials = JSON.stringify(credentials);
		// prompt("4");	

	// }
	// debugger

	// console.log("before timer");
	// setTimeout(() => {
	// 	console.log("timer");
	// 	// debugger
	// 	document.location.href = "index.html";
	//  }, 10000);
// location.reload();
	// console.log("before return");
	// debugger
	// document.location.href = "index.html";
	window.location.href = "/index.html";
	
	// location.reload();

	prompt("5");

	// window.open("index.html", "_self");
}
// )

 

