
// #1

class Worker {
    constructor(nam, snam, r, d ) {
        this.name = nam;
        this.surname = snam;
        this.rate = r;
        this.days = d;

    }
    getSalary() {
    return this.days * this.rate;
    }

}
/*
Worker.prototype.getSalary = function () {
    return this.days * this.rate;
}
*/
const employee1 = new Worker("Іван", "Іванов", 1.2, 24);

document.getElementById("out").innerHTML = `#1 <br>Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище), rate (ставка за день роботи), days (кількість відпрацьованих днів). Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника. Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.<br>`;
document.getElementById("out").innerHTML += `<br> Для робітника ${employee1.name} ${employee1.surname} заробітна платня становить ${employee1.getSalary().toFixed(2)} грн. <hr><br><br>`



// #2

class MyString {
	constructor() {
		// this.str = str;
	}
	myReverse(str) {
		let newString = new String("");
		for (let a = 0; a < str.length; a++) {
			newString += str.slice(str.length - a - 1, str.length - a);
		}
		return newString;
	}

	ucFirst(str) {
		let newString = new String("");
		newString = str[0].toUpperCase();
		for (let a = 1; a < str.length; a++) {
			newString += str.slice(a, a + 1);
		}
		return newString;
	}

    ucWords(str) { 
        let newString = new String("");
				newString = str[0].toUpperCase();
				for (let a = 1; a < str.length; a++) {
					if (str.slice(a, a + 1) === " ") {
						a++;
						newString += " ";
						newString += str.slice(a, a + 1).toUpperCase();
					} else {
						newString += str.slice(a, a + 1);
					}
				}
		return newString;
    }
}
/*
MyString.prototype.myReverse = function (str) {
    let newString = new String("");
    // newString = str.slice(0, str.length);
    // newString.length = 0;
    // newString = str;
    for (let a = 0; a < str.length; a++){
         newString += str.slice(str.length-a-1,str.length-a);
    }
    return newString;
}
MyString.prototype.ucFirst = function (str) {
    let newString = new String("");
    // newString = str.slice(0, str.length);
    newString = str[0].toUpperCase();
    for (let a = 1; a < str.length; a++){
         newString += str.slice(a,a+1);
    }
    return newString;
}
MyString.prototype.ucWords = function (str) {
    let newString = new String("");
    // newString = str.slice(0, str.length);
    newString = str[0].toUpperCase();
    for (let a = 1; a < str.length; a++){
        if (str.slice(a, a + 1) === ' ') {
            a++;
            newString += " ";
            newString +=str.slice(a, a + 1).toUpperCase();
        } else {
            newString += str.slice(a, a + 1);
        }    
    }
    return newString;
}
*/

const testString = new MyString();

document.getElementById("out").innerHTML += `#2 <br> Реалізуйте клас MyString, який матиме такі методи: метод reverse(), який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(), який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.<br><br>`;

document.getElementById("out").innerHTML += `#2.1 Вихідній рядок "qwertyuiop", результат ${testString.myReverse("qwertyuiop")} <br>`;
document.getElementById("out").innerHTML += `#2.2 Вихідній рядок "qwertyuiop", результат ${testString.ucFirst("qwertyuiop")} <br>`;
document.getElementById("out").innerHTML += `#2.3 Вихідній рядок "qwer tyu iop ddd", результат ${testString.ucWords("qwer tyu iop ddd")} <br><hr><br><br>`;

// #3

class Phone {
	constructor(num, mod, wght) {
		this.number = num;
		this.model = mod;
		this.weight = wght;
	}

	receiveCall(nam) {
		this.name = nam;
		console.log(`Телефонує ${this.name} `);
    }
    
    getNumber(){
        console.log(`${this.number}`);
        return this.number;
    }
}
/*
Phone.prototype.receiveCall = function(nam){
    this.name = nam;
    console.log(`Телефонує ${this.name} `);
}
Phone.prototype.getNumber = function(){
    console.log(`${this.number}`);
    return this.number;
}
*/

const phone1 = new Phone("555-2343", "Xiaomi S4", 0.435);
const phone2 = new Phone("451-0043", "Samsung S20", 0.500);
const phone3 = new Phone("111-2222", "Nokia 2140", 1.5);

console.dir(phone1);
console.dir(phone2);
console.dir(phone3);

phone1.receiveCall("Ivan");
phone2.receiveCall("Vasyl");
phone3.receiveCall("Petro");

phone1.getNumber();
phone2.getNumber();
phone3.getNumber();

document.getElementById("out").innerHTML += `#3 <br> Створіть клас Phone, який містить змінні number, model і weight. Створіть три екземпляри цього класу. Виведіть на консоль значення їх змінних. Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.<br><br>`;

document.getElementById("out").innerHTML += `Вивід зроблено на консоль <br><hr><br><br>`;


// #4
document.getElementById("out").innerHTML += `#4 <br> Створити клас Car, Engine та Driver. Клас Driver містить поля - ПІБ, стаж водіння. Клас Engine містить поля – потужність, виробник. Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна. Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова. Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.<br><br>`;


class Driver{
    constructor(nam, driv) {
        this.name = nam;
        this.drivingExperience = driv;
    }

    toString() {
    document.getElementById("out").innerHTML += `ПІБ водія: ${this.name} <br>стаж водіння: ${this.drivingExperience} <br>`;
    }    
}
/*
Driver.prototype.toString = function () {
    document.getElementById("out").innerHTML += `ПІБ водія: ${this.name} <br>стаж водіння: ${this.drivingExperience} <br>`;
}    
*/

class Engine{
    constructor(pow, orig) {
        this.power = pow;
        this.origin = orig;
    }
 
    toString() {
    document.getElementById("out").innerHTML += `двигун: ${this.power} вироблено в ${this.origin}<br>`;
    }
}
/*
Engine.prototype.toString = function () {
    document.getElementById("out").innerHTML += `двигун: ${this.power} вироблено в ${this.origin}<br>`;
}
*/
class Car {
    constructor(mod, clas, wgh, driv, eng) {
        this.model = mod;
        this.class = clas;
        this.weight = wgh;
        this.driver = driv;
        this.engine = eng;
    }

    start() {
        document.getElementById("out").innerHTML += `${this.model} Поїхали <br>`;
    }

    stop() {
            document.getElementById("out").innerHTML += `${this.model} Зупиняємося <br>`;
        }

    turnRight() {
            document.getElementById("out").innerHTML += `${this.model} Поворот праворуч <br>`;
        }

    turnLeft() {
            document.getElementById("out").innerHTML += `${this.model} Поворот ліворуч <br>`;
        }

    toString() {
            document.getElementById("out").innerHTML += `Автомобіль: ${this.model} <br>клас: ${this.class} <br>вага: ${this.weight} <br>двигун: ${this.engine.power} ${this.engine.origin} <br>водій: ${this.driver.name} ${this.driver.drivingExperience} <br><br>`;
        }

}
/*
Car.prototype.start = function () {
        document.getElementById("out").innerHTML += `${this.model} Поїхали <br>`;
    }
Car.prototype.stop = function () {
        document.getElementById("out").innerHTML += `${this.model} Зупиняємося <br>`;
    }
Car.prototype.turnRight = function () {
        document.getElementById("out").innerHTML += `${this.model} Поворот праворуч <br>`;
    }
Car.prototype.turnLeft = function () {
        document.getElementById("out").innerHTML += `${this.model} Поворот ліворуч <br>`;
    }
Car.prototype.toString = function () {
        document.getElementById("out").innerHTML += `Автомобіль: ${this.model} <br>клас: ${this.class} <br>вага: ${this.weight} <br>двигун: ${this.engine.power} ${this.engine.origin} <br>водій: ${this.driver.name} ${this.driver.drivingExperience} <br><br>`;
    }
*/


// document.getElementById("out").innerHTML += `#4 <br><br>`;


const driver1 = new Driver("Ivanov Ivan", 15);
const engine1 = new Engine(185, "Japan");

const car1 = new Car("Toyota", "sedan", 1840, driver1, engine1);

console.dir(car1);

car1.start();
car1.turnLeft();
car1.turnRight();
car1.stop();
car1.toString();

class Lorry extends Car {
    constructor(mod, clas, wgh, driv, eng, load) {
        super(mod, clas, wgh, driv, eng);
        this.loading = load;
    }
}

class SportCar extends Car{
    constructor(mod, clas, wgh, driv, eng, maxSpeed) {
        super(mod, clas, wgh, driv, eng);
        this.speedLimit = maxSpeed;
    }
}

const lorry1 = new Lorry("MAN", "sedan", 1840, driver1, engine1, 2000);

const sports1 = new SportCar ("McClaren", "sedan", 1840, driver1, engine1, 500);

lorry1.start();
lorry1.toString();
console.dir(lorry1);

sports1.stop();
sports1.toString();
console.dir(sports1);

document.getElementById("out").innerHTML += `<br>Вивід частково зроблено на консоль <br><hr><br><br>`

// #5

document.getElementById("out").innerHTML += `#5<br>`;
document.getElementById("out").innerHTML += `Створити клас Animal та розширюючі його класи Dog, Cat, Horse. Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить". Dog, Cat, Horse перевизначають методи makeNoise, eat. Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин. Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом. У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас.У циклі надсилайте їх на прийом до ветеринара.<br><br>`;

class Animal{
    constructor(name, food, location) {
        this.name = name;
        this.food = food;
        this.location = location;
    }

    makeNoise() {
    console.log(`Така тварина кричить`);
    }

    eat() {
        console.log(`Така тварина їсть`);
    }

    sleep() {
        console.log(`Така тварина спить`);    
    }
}

/*
Animal.prototype.makeNoise = function () { console.log(`Така тварина кричить`);}
Animal.prototype.eat = function () { console.log(`Така тварина їсть`);}
Animal.prototype.sleep = function () { console.log(`Така тварина спить`); }
*/

const animal1 = new Animal("тварина", "їжа", "Україна");

console.dir(animal1);
document.getElementById("out").innerHTML += `animal1, ${animal1.name} їсть ${animal1.food}, живе ${animal1.location} <br><br>`;
console.log(`animal1, ${animal1.name} їсть ${animal1.food}, живе ${animal1.location}`);
animal1.makeNoise();
animal1.eat();
animal1.sleep();

class Cat extends Animal {
	constructor(name, food, location, noise, age) {
        super(name, food, location);
        this.noise = noise;
		this.age = age;
    }
    makeNoise() {
        document.getElementById("out").innerHTML += `Кіт каже ${this.noise}<br>`;
        console.log(`Кіт каже ${this.noise}`);
    };
    eat() {
        document.getElementById("out").innerHTML += `Кіт їсть ${this.food} <br><br>`;
        console.log(`Кіт їсть ${this.food}`);
    };
}
/*
Cat.prototype.makeNoise = function () {console.log(`Кіт каже Мяу-мяу`);}
Cat.prototype.eat = function () { console.log(`Кіт їсть Миші`);};
*/
const cat1 = new Cat("Кіт", "ковбаса", "Горище","Мяу",5)
    
console.dir(cat1);
document.getElementById("out").innerHTML +=`cat1, ${cat1.name} їсть ${cat1.food}, живе ${cat1.location}, вік ${cat1.age} <br>`;
console.log(`cat1, ${cat1.name} їсть ${cat1.food}, живе ${cat1.location}, вік ${cat1.age}`);
cat1.makeNoise();
cat1.eat();

class Dog extends Animal {
    constructor(name, food, location, noise, breed) {
        super(name, food, location);
        this.noise = noise;
        this.breed = breed;
    }

    makeNoise() {
        document.getElementById("out").innerHTML += `Собака каже ${this.noise} <br>`;
    	console.log(`Собака каже ${this.noise}`);
    };
    eat() {
        document.getElementById("out").innerHTML += `Собака гризе ${this.food} <br><br>`;
        console.log(`Собака гризе ${this.food}`);
    };
}

/*
Dog.prototype.makeNoise = function () {	console.log(`Собака каже ${this.noise}`);};
Dog.prototype.eat = function () {console.log(`Собака гризе ${this.food}`);};
*/

const dog1 = new Dog("Собака", "кістка", "Будка", "Гав", "шпіц");

console.dir(dog1);
document.getElementById("out").innerHTML += `dog1, ${dog1.name} їсть ${dog1.food}, живе ${dog1.location}, порода ${dog1.breed} <br>`;
console.log(`dog1, ${dog1.name} їсть ${dog1.food}, живе ${dog1.location}, порода ${dog1.breed}`);
dog1.makeNoise();
dog1.eat();

class Horse extends Animal{
    constructor(name,food, location, noise, color) {
        super(name, food, location);
        this.noise = noise;
        this.color = color;
    }
    makeNoise() {
        document.getElementById("out").innerHTML += `Кінь каже ${this.noise} <br>`;
        console.log(`Кінь каже ${this.noise}`);
    };
    eat() {
        document.getElementById("out").innerHTML += `Кінь їсть ${this.food} <br><br>`;
        console.log(`Кінь їсть ${this.food}`);
    };
}

/*
Horse.prototype.makeNoise = function () {console.log(`Кінь каже І-го-го`);};
Horse.prototype.eat = function () {	console.log(`Кінь їсть зерно`);};
*/

const horse1 = new Horse("Кінь","зерно", "Стайні", "І-го-го", "вороний");

console.dir(horse1);
document.getElementById("out").innerHTML += `horse1, ${horse1.name} їсть ${horse1.food}, живе ${horse1.location}, колір ${horse1.color} <br>`;
console.log(`horse1, ${horse1.name} їсть ${horse1.food}, живе ${horse1.location}, колір ${horse1.color}`);
horse1.makeNoise();
horse1.eat();

class Veterinary {
    constructor() {
    }

    treatAnimal(an) {
        this.animal = an;
        document.getElementById("out").innerHTML += `${this.animal.name}, їсть ${this.animal.food}, живе ${this.animal.location} <br>`;
        console.log(`${this.animal.name}, їсть ${this.animal.food}, живе ${this.animal.location}`);
    }

    main() {
        let arr1 = new Array(Animal);
        arr1 = [cat1, dog1, horse1];

        for (let patient of arr1) {
            this.treatAnimal(patient);
        }
    }
}

const vet1 = new Veterinary();
console.log(`treatAnimal кіт`);
vet1.treatAnimal(cat1);

console.log(`цикл запису`);
document.getElementById("out").innerHTML += `<br>Запис до ветеринара <br><br>`;
vet1.main();

document.getElementById("out").innerHTML += `<br>Вивід частково зроблено на консоль <br><hr><br><br>`;