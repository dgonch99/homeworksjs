
/* Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата".
Создать в объекте вложенный объект - "Приложение".
Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата".
Создать методы для заполнения и отображения документа.
*/


'use strict'

const myDocument = {
    header: "0",
    body: "0",
    footer: "0",
    date: "0",
    application: {
        header: { headerText: "0" },
        body: { bodyText: "0" },
        footer: { footerText: "0", footerText2: "0" },
        date: { dateText: "0", example: function() {let q = 1 } },
    },
  
    fill_in: function () {
        for (let a in this) {
            if (typeof (this[a]) === 'string') {
                this[a] = prompt(`Введіть значення властивості ${a}`);
            } else {
                if (typeof (this[a]) === 'object') {
                    // for (let b in myDocument.application) {
                    for (let b in this[a]) {
                        if (typeof (this[a][b]) === 'string') {
                            this[a][b] = prompt(`Введіть значення властивості ${a}.${b}`);
                        } else{
                            if (typeof (this[a][b]) === 'object') {
                                for (let c in this[a][b]) {
                                    if (typeof (this[a][b][c]) === 'string') {
                                        this[a][b][c] = prompt(`Введіть значення властивості myDocument.${[a]}.${[b]}.${[c]}`);
                                    }
                                    // else this[a][b][c] = '0'
                                }
                            } 
                        }
                    }
                }
            }
        }
    },

       
    putOnScrteen: function () {
        for (let a in this) {
            if (typeof (this[a]) === 'string') {
                document.getElementById("out").innerHTML += `Значення властивості myDocument.${a} : ${this[a]} </br>`;
            } else {
                if (typeof (this[a]) === 'object') {
                    for (let b in this[a]) {
                        if (typeof (this[a][b]) === 'string') {
                            document.getElementById("out").innerHTML += `Значення властивості myDocument.${[a]}.${[b]} : ${this[a][b]} </br>`;
                        } else {
                            if (typeof (this[a][b]) === 'object') {
                                for (let c in this[a][b]) {
                                    if (typeof (this[a][b][c]) === 'string') {
                                        document.getElementById("out").innerHTML += `Значення властивості myDocument.${[a]}.${[b]}.${[c]} : ${this[a][b][c]} </br>`;
                                    }
                                    else {
                                        document.getElementById("out").innerHTML += `Значення властивості myDocument.${[a]}.${[b]}.${[c]} : ${typeof (this[a][b][c])} </br>`;
                                    }
                                }
                            } 
                        }
                    }
                }
            }
        }
    }

    
    
}

// document.getElementById("out").innerHTML = `START`;

// document.getElementById("out").innerHTML += ` ${Object.values(myDocument)} </br>`;
// document.getElementById("out").innerHTML += ` ${Object.entries(myDocument)} </br>`;

 document.getElementById("out").innerHTML = `#1 </br> Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата".
        Создать в объекте вложенный объект - "Приложение".
        Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата".
        Создать методы для заполнения и отображения документа. </br></br>`;
        document.getElementById("out").innerHTML += `Об'єкт з ім'ям document створити не можна, це одне з зарезервованих слів, поточне окно виведення браузера. Використано близьке ім'я myDocument. Властивість footerText2 та функція example добавлені для перевірки логіки вводу-виводу строкових/нестрокових властивостей вкладеного об'єкта</br></br>`;
        

myDocument.fill_in();
                
myDocument.putOnScrteen();

document.getElementById("out").innerHTML += `</br> <hr/>`;


