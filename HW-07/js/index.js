// #1
// Написати функцію`filterBy()`, яка прийматиме 2 аргументи.Перший аргумент - масив, який міститиме будь - які дані, другий аргумент - тип даних.
//  Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].


const arr1 = ["hello", "WORLD", 23, 2.334, null, undefined, NaN, Infinity, true, false, { name: "Ivan", age: 25 }];
let arr2 = [];

const dataType = ['number', 'string', 'boolean', 'null', 'undefined', 'object'];

function filterBy(arr, filterString) {
    
    let newArray = new Array("");
    
    for (let b=0, a = 0; a < arr.length; a++) {
            // console.log(filterString, a, arr[a], typeof arr[a]);
        switch (typeof arr[a]) {
            case "number":
            case "string":
            case "boolean":
            case "undefined":{
                if (typeof arr[a] !== filterString) {
                    newArray[b] = arr[a];
                    b++;
                }
                break;
            }
            case "object": {
                if ((arr[a] === null) && (filterString === 'null')) {
                    continue;
                } else if (filterString !== 'object') {
                    newArray[b] = arr[a];
                    b++;
                    // break;
                }    
                if ((arr[a] === null) && (filterString === 'object')) {
                    newArray[b] = arr[a];
					b++;
                }
                break;
            }
        }   
    
    }
	return newArray;
}



console.log("Початковий масив", arr1);
for (let c = 0; c < dataType.length; c++) {
    arr2 = filterBy(arr1, dataType[c]);
    console.log("Фільтр ", dataType[c], ", отримуємо масив: ", arr2);
}


             // if ((arr[a] === null) && (filterString === 'null')) {
                //     continue;
                // } else if (filterString !== 'object') {
                //     newArray[b] = arr[a];
                //     b++;



// #2

// Задача 2 Переписати гру шибениця з книги на новий синтасис.

function hangs() {
    // Создаем массив со словами
    const words = [
        "программа",
        "макака",
        "прекрасный",
        "оладушек"
    ];
    // Выбираем случайное слово
    const word = words[Math.floor(Math.random() * words.length)];
    // Создаем итоговый массив
    let answerArray = [];
    for (let i = 0; i < word.length; i++) {
        answerArray[i] = "_";
    }
    let remainingLetters = word.length;
    // Игровой цикл
    while (remainingLetters > 0) {
        // Показываем состояние игры
        alert(`Слово ${answerArray.join(" ")}`);
        // Запрашиваем вариант ответа
        let guess = prompt(`Угадайте букву, или нажмите Отмена для выхода из игры. Осталось ${remainingLetters} букв`);
        if (guess === null) {
            // Выходим из игрового цикла
            break;
        } else if (guess.length !== 1) {
            alert(`Пожалуйста, введите одиночную букву.`);
        } else {
            // Обновляем состояние игры
            for (let j = 0; j < word.length; j++) {
                if (word[j] === guess) {
                    answerArray[j] = guess;
                    remainingLetters--;
                }
            }
        }
        // Конец игрового цикла
    }
    // Отображаем ответ и поздравляем игрока
    alert(answerArray.join(" "));
    alert(`Отлично! Было загадано слово ${word}`);
}

hangs();
