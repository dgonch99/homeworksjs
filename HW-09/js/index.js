// ДЗ : Згенерувати теги теги через javascript. Додати на сторінку семантичні теги та метагеги опису сторінки.
//  прописати стилі для для елементів використовуючи css id та класи
//  при натиску на тег ми можемо доти будь-який контент і він зберігається в тегу

window.onload = function () {
	// let body = document.getElementsByTagName("body");

	let body = document.querySelector("body");
	body.classList.add("body1");
	body.setAttribute("id", "body1");
	body.textContent = "BODY";
	// body.style.textAlign = "center";

	let header1 = document.createElement("header");
	header1.classList.add("header");
	header1.setAttribute("id", "header");
	header1.textContent = "HEADER";
	// console.dir(header1);
	body.append(header1);

	let headerNav = document.createElement("nav");
	headerNav.classList.add("header_nav");
	headerNav.setAttribute("id", "header_nav");
	headerNav.textContent = "NAV";
	header1.append(headerNav);

	let divSections = document.createElement("div");
	divSections.classList.add("main_part");
	// divSections.setAttribute("id", "text");
	divSections.style.display = "flex";
	body.append(divSections);

	let sectionLeft = document.createElement("section");
	sectionLeft.classList.add("section_left");
	sectionLeft.textContent = "SECTION left";
	divSections.append(sectionLeft);

	let sectionLHeader1 = document.createElement("header");
	sectionLHeader1.classList.add("section_left_header1");
	// sectionLHeader1.setAttribute("id", "text");
	sectionLHeader1.textContent = "Section left HEADER";
	sectionLeft.append(sectionLHeader1);

	let sectionLArticle1 = document.createElement("article");
	sectionLArticle1.classList.add("section_left_article1");
	// sectionLArticle1.setAttribute("id", "text");
	sectionLArticle1.textContent = "Section left Article1";
	sectionLeft.append(sectionLArticle1);

	let sectionLHeader2 = document.createElement("header");
	sectionLHeader2.classList.add("section_left_header2");
	// sectionLHeader2.setAttribute("id", "text");
	sectionLHeader2.textContent = "Section left Article1 HEADER";
	sectionLArticle1.append(sectionLHeader2);

	let sectionLp1 = document.createElement("p");
	sectionLp1.classList.add("section_left_p1");
	// sectionLp1.setAttribute("id", "text");
	sectionLp1.textContent = "Section left Article1 P1";
	sectionLArticle1.append(sectionLp1);

	let divArticle = document.createElement("div");
	divArticle.classList.add("section_left_art");
	divArticle.style.display = "flex";
	sectionLArticle1.append(divArticle);

	let sectionLp2 = document.createElement("p");
	sectionLp2.classList.add("section_left_p2");
	// sectionLp2.setAttribute("id", "text");
	sectionLp2.textContent = "Section left Article1 P2";
	divArticle.append(sectionLp2);

	let sectionLAside = document.createElement("aside");
	sectionLAside.classList.add("section_left_aside");
	// sectionLAside.setAttribute("id", "text");
	sectionLAside.textContent = "Section left Article1 ASIDE";
	// console.dir(header1);
	divArticle.append(sectionLAside);

	let sectionLFooter1 = document.createElement("footer");
	sectionLFooter1.classList.add("section_left_footer1");
	// sectionLFooter1.setAttribute("id", "text");
	sectionLFooter1.textContent = "Section left Article1 FOOTER";
	sectionLArticle1.append(sectionLFooter1);

	let sectionLArticle2 = document.createElement("article");
	sectionLArticle2.classList.add("section_left_article2");
	// sectionLArticle2.setAttribute("id", "text");
	sectionLArticle2.textContent = "Section left Article2";
	sectionLeft.append(sectionLArticle2);

	let sectionLHeader3 = document.createElement("header");
	sectionLHeader3.classList.add("section_left_header3");
	// sectionLHeader3.setAttribute("id", "text");
	sectionLHeader3.textContent = "Section left Article2 HEADER";
	sectionLArticle2.append(sectionLHeader3);

	let sectionLp3 = document.createElement("p");
	sectionLp3.classList.add("section_left_p3");
	// sectionLp3.setAttribute("id", "text");
	sectionLp3.textContent = "Section left Article2 P1";
	sectionLArticle2.append(sectionLp3);

	let sectionLp4 = document.createElement("p");
	sectionLp4.classList.add("section_left_p4");
	// sectionLp4.setAttribute("id", "text");
	sectionLp4.textContent = "Section left Article2 P2";
	sectionLArticle2.append(sectionLp4);

	let sectionLFooter2 = document.createElement("footer");
	sectionLFooter2.classList.add("section_left_footer2");
	// sectionLFooter2.setAttribute("id", "text");
	sectionLFooter2.textContent = "Section left Article2 FOOTER";
	sectionLArticle2.append(sectionLFooter2);

	let sectionLFooter3 = document.createElement("footer");
	sectionLFooter3.classList.add("section_left_footer3");
	// sectionLFooter3.setAttribute("id", "text");
	sectionLFooter3.textContent = "Section left FOOTER";
	sectionLeft.append(sectionLFooter3);

	let bodyFooter = document.createElement("footer");
	bodyFooter.classList.add("footer");
	// bodyFooter.setAttribute("id", "text");
	bodyFooter.textContent = "BODY FOOTER";
	body.append(bodyFooter);

	let sectionRight = document.createElement("section");
	sectionRight.classList.add("section_right");
	// sectionRight.setAttribute("id", "text");
	sectionRight.textContent = "SECTION right";
	divSections.append(sectionRight);

	let sectionRHeader2 = document.createElement("header");
	sectionRHeader2.classList.add("section_right_header");
	// sectionRHeader2.setAttribute("id", "text");
	sectionRHeader2.textContent = "Section right HEADER";
	// console.dir(header1);
	sectionRight.append(sectionRHeader2);

	let sectionRNav1 = document.createElement("nav");
	sectionRNav1.classList.add("nav_right");
	// sectionRNav1.setAttribute("id", "text");
	sectionRNav1.textContent = "Section right NAV";
	// console.dir(header1);
	sectionRight.append(sectionRNav1);

	// const all = document.querySelectorAll("#text");
	// console.dir(all);

	document.onclick = function (event) {
		// console.dir(event);
				
		let target = event.target;
		console.dir(target.childNodes[0].textContent);
		//  debugger;
		
		// target.firstChild.wholeText = " TEST TEXT";
		// target.innerText = " TEST TEXT";

		// let eventClass = body.querySelector("." + target.className);
		// console.dir(eventClass);
		// debugger;

		target.childNodes[0].textContent = prompt("Введить текст ", target.childNodes[0].textContent);

		// prompt("Введить текст ");
		// " TEST TEXT";
		// debugger
	};

	

	// let sectionRight = document.querySelector("section");
	// sectionR = `<section class="section_right">
	// 			SECTION right
	// 			<header class="section_right_header">section right HEADER</header>
	// 			<nav class="nav_right">Section right NAV</nav>
	// 			</section>`;
	// body.insertAdjacentHTML("beforeend", sectionR);
}