

// Використовуючи циклічні конструкції, пробіли (&nbsp;) та зірки (*) намалюйте:
// Порожній 
// - прямокутник 
// Заповнений 
// - Рівнобедрений трикутник 
// - Трикутник прямокутний
// - Ромб



const numSteps1 = 10;

let stepCount1 = 0,
    stepCount2 = 0,
    stepCount3 = 0;

    
// Порожній прямокутник 
document.write(`Порожній прямокутник`);
document.write(`<br/>`);
document.write(`<br/>`);
for (stepCount1 = 0; stepCount1 < numSteps1; ++stepCount1)
{
    if (stepCount1 === 0 || stepCount1 === numSteps1 - 1) {
        for (stepCount2 = 0; stepCount2 < numSteps1; ++stepCount2) {
            document.write('*');
        }
        document.write('<br/>');
    } else {
        for (stepCount2 = 0; stepCount2 < numSteps1; ++stepCount2) {
            if (stepCount2 === 0 || stepCount2 === numSteps1 - 1) {
                document.write('*');
            } else {
                document.write(`&nbsp&nbsp`);
            }
        }
        document.write('<br/>');
    }
}
document.write(`<br/>`);


// Порожній прямокутник 
document.write(`Порожній прямокутник 2`);
document.write(`<br/>`);
document.write(`<br/>`);
for (stepCount1 = 0; stepCount1 < numSteps1*2; ++stepCount1)
{
    if (stepCount1 === 0 || stepCount1 === numSteps1*2-1 ) {
        for (stepCount2 = 0; stepCount2 < numSteps1+1; ++stepCount2) {
            document.write('*');
        }
        document.write('<br/>');
    } else {
        for (stepCount2 = 0; stepCount2 < numSteps1*2; ++stepCount2) {
            if (stepCount2 === 0 || stepCount2 === numSteps1*2 - 1) {
                document.write('*');
            } else {
                document.write(`&nbsp`);
            }
        }
        document.write('<br/>');
    }
}
document.write(`<br/>`);


// Заповнений Рівнобедрений трикутник 
stepCount1 = 0;
stepCount2 = 0;
document.write(`Заповнений Рівнобедрений трикутник`);
document.write(`<br/>`);
for (stepCount1 = 0; stepCount1 < numSteps1; ++stepCount1)
{
    for (stepCount2 = stepCount1; stepCount2 < numSteps1; ++stepCount2)
    {
        document.write(`&nbsp`);
    }
    for (stepCount2 = numSteps1 - stepCount1; stepCount2 < numSteps1; ++stepCount2)
    {
        document.write(`*`);    
    }
    document.write(`<br/>`);

}
document.write(`<br/>`);


// Заповнений Рівнобедрений трикутник 
stepCount1 = 0;
stepCount2 = 0;
document.write(`Заповнений Рівнобедрений трикутник 2`);
document.write(`<br/>`);
for (stepCount1 = 0; stepCount1 < numSteps1; ++stepCount1)
{
    for (stepCount2 = numSteps1 - stepCount1; stepCount2 < numSteps1; ++stepCount2)
    {
        document.write(`*`);    
    }
    document.write(`<br/>`);
}
for (stepCount1 = numSteps1-1; stepCount1 > 0; --stepCount1)
{
    for (stepCount2 = stepCount1-1; stepCount2 >0; --stepCount2)
    {
        document.write(`*`);    
    }
    document.write(`<br/>`);
}
document.write(`<br/>`);




// Заповнений Трикутник прямокутний 
stepCount1 = 0;
stepCount2 = 0;
document.write(`Заповнений Трикутник прямокутний 1 - FOR`);
document.write(`<br/>`);

for (stepCount1 = 0; stepCount1 < numSteps1; ++stepCount1)
{
    for (stepCount2 = numSteps1-stepCount1; stepCount2 < numSteps1;++stepCount2)
            document.write('*');
    document.write('<br/>');

}
document.write(`<br/>`);


// Заповнений Трикутник прямокутний 
stepCount1 = 0;
stepCount2 = 0;
document.write(`Заповнений Трикутник прямокутний 2 - DO WHILE`);
document.write(`<br/>`);
do {
    stepCount2 = numSteps1 - stepCount1;
    while (stepCount2 < numSteps1)
    {
        document.write('*');
        ++stepCount2;
        }
        ++stepCount1;
        document.write('<br/>');
} while (stepCount1<numSteps1);
document.write(`<br/>`);


// Заповнений Трикутник прямокутний 
stepCount1 = 0;
stepCount2 = 0;
document.write(`Заповнений Трикутник прямокутний 3 FOR`);
document.write(`<br/>`);
document.write(`<br/>`);
for (stepCount1 = numSteps1; stepCount1 > 0; --stepCount1)
{
    for (stepCount2 = numSteps1-stepCount1; stepCount2 < numSteps1;++stepCount2)
            document.write('*');
    document.write('<br/>');

}
document.write(`<br/>`);

// Заповнений Трикутник прямокутний 
stepCount1 = 0;
stepCount2 = 0;
document.write(`Заповнений Трикутник прямокутний 4 DO WHILE`);
document.write(`<br/>`);
do {
   stepCount3 = 0;
    while (numSteps1-stepCount1 > stepCount3)
    {
        document.write(`&nbsp&nbsp`);
        ++stepCount3;
    }  
    stepCount2 = numSteps1 - stepCount1;
    while (stepCount2 < numSteps1)
    {
        document.write('*');
        ++stepCount2;
    }
    ++stepCount1;
    document.write('<br/>');
} while (stepCount1 < numSteps1);
document.write(`<br/>`);


// Заповнений Ромб 
stepCount1 = 0;
stepCount2 = 0;
stepCount3 = 0;
document.write(`Заповнений Ромб 1`);
document.write(`<br/>`);
document.write(`<br/>`);
for (stepCount1 = 0; stepCount1 < numSteps1; ++stepCount1)
{
    for (stepCount2 = stepCount1; stepCount2 < numSteps1;++stepCount2)
    {
        document.write(`&nbsp&nbsp`);
    }
    for (stepCount3 = numSteps1 - stepCount2; stepCount3 < numSteps1*1.6;++stepCount3 )
    { 
        document.write(`*`); 
    }
    document.write(`<br/>`);
}
document.write(`<br/>`);




// Заповнений Ромб 
stepCount1 = 0;
stepCount2 = 0;
document.write(`Заповнений Ромб 2`);
document.write(`<br/>`);
for (stepCount1 = 0; stepCount1 < numSteps1; ++stepCount1)
{
    for (stepCount2 = stepCount1; stepCount2 < numSteps1; ++stepCount2)
    {
        document.write(`&nbsp`);
    }
    for (stepCount2 = numSteps1 - stepCount1; stepCount2 < numSteps1; ++stepCount2)
    {
        document.write(`*`);    
    }
    document.write(`<br/>`);
}
for (stepCount1 = 0; stepCount1 < numSteps1; ++stepCount1)
{
    for (stepCount2 = 0; stepCount2 < stepCount1; ++stepCount2)
    {
        document.write(`&nbsp`);
    }
    for (stepCount2 = stepCount1; stepCount2 < numSteps1; ++stepCount2)
    {
        document.write(`*`);    
    }
    document.write(`<br/>`);
}
document.write(`<br/>`);

