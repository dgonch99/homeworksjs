import {userSlectTopping} from "./functions.js"

export function clickInputSize(e) {
    if(e.target.tagName === "INPUT"){
        userSlectTopping(e.target.value)
    }
}

export const clickToppingAdd = function (e) {
    if (e.target.tagName === "IMG") {
        userSlectTopping(e.target.id)
    }
}

export const clickToppingAddPlus = function (e) {
	if (e.target.tagName === "IMG") {
		// var anchor = e.target.parentElement;
		// console.dir(anchor)
		// var cloneSource = e.explicitOriginalTarget.cloneNode(true);
		// anchor.append(cloneSource);
		
		// cloneSource.id = e.target.id + "-1";
		// cloneSource.hidden = true;
		// debugger
        
			// * e.target.style.opacity = "0.5";
		e.dataTransfer.effectAllowed = "move";
		e.dataTransfer.setData("Text", e.target.id);
		
        var source = e.explicitOriginalTarget;

        source.addEventListener("dragstart", function (e) {
       
            // this.style.opacity = "0.7";
            e.dataTransfer.effectAllowed = "move";
		
			e.dataTransfer.setData("pizza_target", e.target.id);
        
            },false);

        source.addEventListener("dragend", function (e) {

			// * e.target.style.opacity = "0.5"; 
         }, false);
        
        
        var target = document.getElementById("pizza_target");

		target.addEventListener("dragenter",function (e) {
			// * e.target.style.opacity = "0.5";
			console.dir("dragenter");
		}, false);
		
		target.addEventListener("dragover",	function (e) {
			if (e.preventDefault) e.preventDefault();
			console.dir("dragover");
			return false;
		},false);
        
        target.addEventListener("drop",	function (e) {
			if (e.preventDefault) e.preventDefault();
			if (e.stopPropagation) e.stopPropagation();

			e.target.style.opacity = "0.5";

			// debugger;

			var id = e.dataTransfer.getData("Text");
			if (!id) {
				console.dir("id  The data transfer contains no text data", id);
			}
			
			target.before(document.getElementById(id));

			document.getElementById(id).classList.add("dragged");

			return false;
		}, false);
        
		userSlectTopping(e.target.id);
		
	}
};


export const clickSauceAdd = (e)=> {
    if(e.target.tagName === "IMG"){
        userSlectTopping(e.target.id)
    }
}

export const clickSauceAddPlus = e => {
    if (e.target.tagName === "IMG") {

		// e.target.style.opacity = "0.5";
		e.dataTransfer.effectAllowed = "move";
		e.dataTransfer.setData("Text", e.target.id);

		var source = e.explicitOriginalTarget;

		source.addEventListener("dragstart", function (e) {
			e.dataTransfer.effectAllowed = "move";
			e.dataTransfer.setData("pizza_target", e.target.id);

		},false);

		source.addEventListener("dragend", function (e) {
				// e.target.style.opacity = "0.5";
			},	false);

		var target = document.getElementById("pizza_target");
		target.addEventListener("dragenter",function (e) {
				// e.target.style.opacity = "0.5";
			},false);
		target.addEventListener("dragover",	function (e) {
				if (e.preventDefault) e.preventDefault();
				return false;
			}, false);

		target.addEventListener("drop",	function (e) {

				if (e.preventDefault) e.preventDefault();
				if (e.stopPropagation) e.stopPropagation();

				e.target.style.opacity = "0.5";

				var id = e.dataTransfer.getData("Text");
				if (!id) {
					console.dir("id  The data transfer contains no text data", id);
				}
				// var targetDiv = document.getElementById("target");
				target.before(document.getElementById(id));
				document.getElementById(id).classList.add("dragged");

				return false;
			},false);

		// debugger
		userSlectTopping(e.target.id);
		}
};


