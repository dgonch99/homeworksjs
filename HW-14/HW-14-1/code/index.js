/*
 Виконати запит на https://swapi.dev/api/people (https://starwars-visualguide.com) отримати список героїв зіркових воєн.

            Вивести кожного героя окремою карткою із зазначенням. картинки Імені, статевої приналежності, ріст, колір шкіри,
            рік народження та планету на якій народився.
           
            Створити кнопку зберегти на кожній картці. 
            При натисканні кнопки записуйте інформацію у браузері
*/

// window.addEventListener("DOMContentLoaded", function () {
const body = document.querySelector("body");
    const loader = document.querySelector(".box-loader");
    const heroes = [];
	
    function getData(makeCard) {
        // debugger;
        loader.classList.add("loader");
        const starWarData = new XMLHttpRequest();
        starWarData.open("get", "https://swapi.dev/api/people");
        starWarData.send();
        
        // console.dir("state", starWarData.readyState);
        // console.dir("status", starWarData.status);
        // let dataSet = JSON.parse(starWarData.response);
        // console.dir(dataSet.results);
        // makeCard(dataSet.results);
        // debugger;
        starWarData.addEventListener("readystatechange", () => {
					// debugger;
					if (starWarData.readyState === 4 && starWarData.status >= 200 && starWarData.status < 300) {
						// console.dir("state in", starWarData.readyState);

						let dataSet = JSON.parse(starWarData.response);
						
						makeCard(dataSet.results);
						// console.dir(dataSet.results);
                        // debugger
                        showHero(heroes);
                        loader.classList.remove("loader");
                        
					} else if (starWarData.readyState === 4) {
						console.log(`Помилка: ${starWarData.status} ${starWarData.statusText}`);
					}
				});
    }
 


function makeHero(set = []) {
    set.forEach(({ name, gender, height, skin_color, birth_year, homeworld }, index) => {
        // debugger
        hero = {
					name: name,
					gender: gender,
					height: height,
					skin_color: skin_color,
					birth_year: birth_year,
					homeworld: getPlanet(homeworld),
					picture: `https://starwars-visualguide.com/assets/img/characters/${index+1}.jpg`,
				};
        heroes.push(hero);
        // debugger
        });
    }

    function showHero(set = []) {
            set.forEach((e) => {
        
                const card = `<div class=card>
        <div class=pic><img src=${e.picture} alt=""</img>
        <div class=text1><span>Ім'я: </span>${e.name} </div>
        <div class=text1><span>Пол: </span>${e.gender} </div>
        <div class=text1><span>Зріст: </span>${e.height} </div>
        <div class=text1><span>Колір шкіри: </span>${e.skin_color} </div>
        <div class=text1><span>Рік народження: </span>${e.birth_year} </div>
        <div class=text1><span>Рідна планета: </span>${e.homeworld} </div>
        <input type="button" class="btn" value="Зберегти"> 
         </div>`;
            // getPlanet(homeworld);
                // debugger;
            body.insertAdjacentHTML("beforeEnd", card);

        })
    }    


    function getPlanet(url) {
        const planetData = new XMLHttpRequest();
        planetData.open("get", url, false);
        planetData.send();
       
        let dataSet = JSON.parse(planetData.response);
        
        return dataSet.name;
              
    }

    let inpt = addEventListener("click", (e) => {
		let presKey = e.target;
		// debugger
        if (presKey.type === 'button') { 
            document.body.innerHTML += `${e.target.parentNode.innerText} <br>`;

        }
            
    })


getData(makeHero);
    
    
// })