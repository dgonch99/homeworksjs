/*
Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
    Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
    На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
    сторінок показувати нові коментарі. 
    з коментарів виводити : 
    "id": 1,
    "name"
    "email"
    "body":
    
*/


/*
Створити сторінку коментарів.
Тут взяти коментарі https://jsonplaceholder.typicode.com/comments/ Та вивести на сторінку стилізувавши їх.
*/

const body = document.querySelector(".output");
const paginator = document.querySelector(".pagination");
const allComments = [];
let currentPage = 1;
let startRecord = 0;
let midDisplayedPage = 3;


const comments = new XMLHttpRequest();
comments.open("get", "https://jsonplaceholder.typicode.com/comments/");
    comments.send();
comments.addEventListener("readystatechange", () => {
    if (comments.readyState === 4 && comments.status >= 200 && comments.status < 300) {
        
        // console.dir(comments.response);

			let dataSet = JSON.parse(comments.response);
        // console.dir(dataSet);
        makeComment(dataSet);
       
        // console.dir(allComments);
      
       

        // showComments(allComments.slice(0, 2).reverse());
          choosePage();
        

			// debugger
			
		} else if (comments.readyState === 4) {
			console.log(`Помилка: ${comments.status} ${comments.statusText}`);
		}
})
    

function makeComment(set = []) {
	set.forEach(({ name, id, body, email }) => {
		// debugger
		comment = {
			name: name,
			body: body,
			email: email,
			id: id,
		};
		allComments.push(comment);
		// debugger
	});
}

function showComments(set = []) {
            set.forEach((e) => {
        
        const card = `<div class=card>
        <div class=text1><span>Id: </span>${e.id} </div>
        <div class=text1><span>Name: </span>${e.name} </div>
        <div class=text1><span>Email: </span><a href="mailto:${e.email}">${e.email}</a> </div>
        <div class=text1><span>Body: </span>${e.body} </div>
        </div>`;
            // getPlanet(homeworld);
                // body.insertAdjacentHTML("beforeEnd", card);
                body.insertAdjacentHTML("afterBegin", card);

        })
    }    


function choosePage() {
    
    // debugger
    paginator.addEventListener("click", evn => {
        console.dir(evn.target);
        // debugger
        if (evn.target.id === "more" && startRecord <= allComments.length) {
			// debugger;
			startRecord = Number(startRecord) + 10;
            currentPage = Number(currentPage) + 1;
		} else if (evn.target.id === "less" && startRecord >= 0) {
			// debugger;
			startRecord = Number(startRecord) - 10 > 0 ? Number(startRecord) - 10 : 0;
			currentPage = Number(currentPage) - 1 > 0 ? Number(currentPage) - 1 : 1;
        } else if (evn.target.className === "page-link") {
            currentPage = Number(evn.target.innerText);
			startRecord = (currentPage - 1) * 10;
        }


        if (midDisplayedPage - Number(document.querySelector(`#${evn.target.id}`).innerText) >= 2) {
            midDisplayedPage = Number(document.querySelector(`#${evn.target.id}`).innerText) - 1 > 2 ? Number(document.querySelector(`#${evn.target.id}`).innerText) - 1 : 3;
            
        } else if (midDisplayedPage - Number(document.querySelector(`#${evn.target.id}`).innerText) <= 2) {midDisplayedPage = Number(document.querySelector(`#${evn.target.id}`).innerText) + 1 < allComments.length/10 - 2 ? Number(document.querySelector(`#${evn.target.id}`).innerText) + 1 : allComments.length / 10 - 2;
            
        }
        // if (midDisplayedPage > allComments.length/10 - 3) {midDisplayedPage = allComments.length/10 - 2;
        // }
        // if (midDisplayedPage <  3) {
		// 			midDisplayedPage = 3;
		// 		}
            // debugger
          
        document.querySelector("#pg1").innerText = midDisplayedPage - 2;
        document.querySelector("#pg2").innerText = midDisplayedPage - 1;
        document.querySelector("#pg3").innerText = midDisplayedPage;
        document.querySelector("#pg4").innerText = midDisplayedPage + 1;
        document.querySelector("#pg5").innerText = midDisplayedPage + 2;
        
        

        // debugger
        // document.querySelectorAll("pg").className.remove(".pagination-focus");
        
        body.innerHTML = "";
        showComments(allComments.slice(startRecord, startRecord+10).reverse());

		console.dir("class", evn.target.className);
		console.dir("id", evn.target.id);
		console.dir("text", evn.target.innerText);
		console.dir("record", startRecord);
        console.dir("page", currentPage);
        console.dir("id", evn.target.innerText);
        console.dir(document.querySelector(`#${evn.target.id}`));
        // debugger
    		
    });
    // debugger;
    showComments(allComments.slice(startRecord, startRecord+10).reverse());
}
