/*
4.
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/
window.addEventListener("DOMContentLoaded", () => {
	inpt = document.querySelector("input");

	message1 = document.createElement("div");
	// message1.innerText = `Якийсь текст та значення поля ${inpt.value}`;
    message1.style.display = "none";
    // message1.style.display = "inline-block";
    message1.style.backgroundColor = "silver";
    message1.style.marginRight = "1rem";
    message1.style.padding = "10px";
    document.getElementById("price").before(message1);

    message2 = document.createElement("div");
		message2.innerText = `Please enter correct price`;
		message2.style.display = "none";
		// message2.style.display = "inline-block";
		// message2.style.backgroundColor = "silver";
		// message2.style.marginRight = "1rem";
		message2.style.padding = "10px";
		inpt.after(message2);

	x1 = document.createElement("div");
    x1.innerText = `X`;
    x1.style.padding = "5px";
    x1.style.backgroundColor = "silver";
    // x1.style.cursor = "pointer";
    // x1.style.display = "inline-block";
    x1.style.display = "none";
    // message1.after(x1);

	inpt.addEventListener("focus", () => {
		// debugger
        inpt.classList.add("green_border");
        inpt.classList.remove("red_border");
		inpt.classList.remove("green_inpt");
        message1.style.display = "none";
         x1.style.display = "none";
        message2.style.display = "none";
	});

	inpt.addEventListener("blur", () => {
		// debugger
        inpt.classList.remove("green_border");
        inpt.classList.remove("red_border");
		inpt.classList.add("green_inpt");
        message1.innerText = `Якийсь текст та значення поля ${inpt.value}`;
        message1.style.display = "inline-block";
		x1.style.display = "inline-block";
         message1.after(x1);
    });
    
    x1.addEventListener("mouseover", () => {
        x1.style.cursor = "pointer";

    })

    x1.addEventListener("mouseout", () => {
			x1.style.cursor = "auto";
		});


    x1.addEventListener("click", () => {
        message1.style.display = "none";
       	x1.style.display = "none";
    })

    inpt.addEventListener("blur", () => {
        if (inpt.value<0) {
            inpt.classList.remove("green_border");
            inpt.classList.add("red_border");
            message1.style.display = "none";
			x1.style.display = "none";
            message2.style.display = "block";
        }

    })

})


