/*
2.
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.

*/

[...inputs] = document.querySelectorAll("input");


inputs.forEach(inp => {
    inp.addEventListener("blur", (e) => {
        // debugger
        if (Number(e.target.value.length) === Number(e.target.dataset.length)) {
					inp.classList.add("green");
					inp.classList.remove("red");
				} else {
					inp.classList.remove("green");
					inp.classList.add("red");
				}
	});

	
});
