/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

window.addEventListener("DOMContentLoaded", () => {

	let pattern1 = /[0-9]/,
		pattern2 = /\./;
	let enteredValue,
		enteredOperation,
		flagOperation = false,
		flagDigit = false,
		flagMemory = false,
		flagStart = false,
		digit = [],
		digit1 = '',
		digit2 = '',
		display='',
		result = '',
		mem = '';
    

	setId = elem => {
		switch (elem.className) {
			case "button black": {
				if (pattern1.test(elem.attributes[2].value)) {
					elem.id = `btn_${elem.attributes[2].value}`;
				} else {
					elem.id = `btn_dot`;
				}
				break;
			}
			case "button pink": {
				if (elem.attributes[2].value === "/") {
					elem.id = `btn_div`;
				} else if (elem.attributes[2].value === "*") {
					elem.id = `btn_mul`;
				} else if (elem.attributes[2].value === "-") {
					elem.id = `btn_min`;
				} else if (elem.attributes[2].value === "+") {
					elem.id = `btn_pls`;
				}
				// elem.id = `btn_${elem.attributes[2].value}`;
				break;
			}
			case "button gray": {
				if (elem.attributes[2].value === "mrc") {
					elem.id = `btn_mrc`;
				} else if (elem.attributes[2].value === "m+") {
					elem.id = `btn_mplus`;
				} else if (elem.attributes[2].value === "m-") {
					elem.id = `btn_mmin`;
				}
				// elem.id = `btn_${elem.attributes[2].value}`;
				break;
			}
			case "button orange": {
				elem.id = `btn_eqv`;
				// elem.removeAttribute("disabled");
				break;
			}
		}

	};

	function calcs(dg1, dg2, oper) {
		switch (enteredOperation) {
			case "/": {
				result = digit1 / digit2;
				break;

			}
			case "*": {
				result = digit1 * digit2;
				break;
                           
			}
			case "-": {
				result = digit1 - digit2;
				break;
                           
			}
			case "+": {
				result = Number(digit1) + Number(digit2);
				break;
			}
                           
		}
		return result;
	}
        


	// debugger

	let buttons = document.querySelectorAll(".button");
	let test = document.getElementsByClassName("button orange");
	// console.dir(test);
	// debugger;

	test[0].disabled = false;
	// test.removeAttribute("disabled");
    
	// console.dir(buttons);
	buttons.forEach(setId);
	// console.dir(buttons);

	let mLetter = document.createElement("div");
	mLetter.style.position = "absolute";
	mLetter.style.left = "10px";
	mLetter.style.top = "5px";
	mLetter.style.color = "black";
	mLetter.style.display = "none";
	mLetter.innerText = "m";
	mLetter.setAttribute("z-index", "10");
	// console.dir(mLetter);
	document.querySelector(".display").append(mLetter);

    
	let disp = document.querySelector(".display input");
	disp.value = "";
	// console.dir(disp);
	// debugger

	let inpt = addEventListener("click", (e) => {
		let presKey = e.target;
		// console.dir(e.target);
		if (presKey.type === 'button') {
	
			// console.dir(presKey);
			// console.dir(presKey.value);
			// console.dir(presKey.className);
			disp.value = ""; //presKey.value;
			switch (presKey.value) {
				case "0":
				case "1":
				case "2":
				case "3":
				case "4":
				case "5":
				case "6":
				case "7":
				case "8":
				case "9":
				case ".": {
					// debugger;
					enteredValue = presKey.value;
					// debugger;
					if (pattern2.test(digit.join("")) && enteredValue === ".") {
						enteredValue = "";
						display = digit.join("");
						disp.value = display;
						break;
					}

					digit.push(enteredValue);
					display = digit.join("");
					disp.value = display;

					flagDigit = true;

					break;
				}

				case "C": {
					flagDigit = false;
					// flagMemory = false;
					flagOperation = false;
					flagEqual = false;
					enteredValue = "";
					digit = [];
					digit1 = "";
					digit2 = "";
					display = '';
					disp.value = display;

					break;
				}

				case "/":
				case "*":
				case "+":
				case "-":
				case "=": {
					debugger;
					if (flagDigit) {
						if (enteredOperation === '=' && presKey.value === '=') {
						// enteredOperation = presKey.value;
						disp.value = display;
						// digit = [];
						// flagOperation = false;
						break;
						}

						if (enteredOperation === '=' && presKey.value !== '=') {
							enteredOperation = presKey.value;
							disp.value = display;
							digit = [];
							flagOperation = true;
							break;
						}

						if (!flagOperation) {
							debugger;
							enteredOperation = presKey.value;
							display = digit.join("");
							disp.value = display;
							digit1 = Number(display);
							digit = [];
							flagOperation = true;
						} else {
							if (digit.length > 0) {
								debugger;
								display =digit.join("");
								disp.value = display;
								digit2 = Number(display);

								result = calcs(digit1, digit2, enteredOperation);

								display = result;
								disp.value = display;

								digit1 = display;
								enteredOperation = presKey.value;
								digit2 = "";
								digit = [];
								if (presKey.value === "=") {
									// flagDigit = false;
									flagOperation = false;
								} else {
									flagOperation = true;
									flagDigit = true;
								}
								// flagDigit = true;
								// flagOperation = true;
							} else {
								debugger;
								enteredOperation = presKey.value;
								disp.value = display;
								flagOperation = true;
							}
						}
					} else {
						debugger
						disp.value = display;  // digit.join("");
						// break;
					}

					// flagMemory = false;

					break;
				}


				case "mrc": {
					if (flagMemory) {
						disp.value = mem;
						digit1 = mem;
						flagMemory = false;
					} else {
						mem = "";
						display = '';
						disp.value = "";
						digit1 = "";
						flagDigit = false;
						mLetter.style.display = "none";
					}

					break;
				}
				case "m+":
				case "m-": {
					// debugger
					mem = display;
					mLetter.style.display = "block";
					// debugger;
					disp.value = display;
					flagMemory = true;

					break;
				}
			}
		}
		})



});



